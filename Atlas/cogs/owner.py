import discord
import asyncio
from discord.ext import commands
from random import choice as randchoice
import time
import requests
import datetime
import rethinkdb as r
from discord.ext.commands.view import StringView
import json
import inspect
from utils import arg, data
import math
import functools
import psutil
from PIL import Image, ImageDraw, ImageFont, ImageOps
from utils import checks
import os

class owner:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(hidden=True)
    @checks.is_owner()
    async def code(self, ctx, *, command: str=None):
        if not command:
            command = ctx.command
        else:
            command = self.bot.get_command(command)
            if not command:
                return await ctx.send("Invalid command :no_entry:")
        wrap = "```py\n{}```"
        code = inspect.getsource(command.callback)
        code = code.replace("```", "\```")
        if len(code) > 1990:
            pages = math.ceil(len(code)/1990)
            n = 0
            m = 1990
            for x in range(pages):
                if n != 0:
                    while code[n-1:n] != "\n":
                        n -= 1
                while code[m-1:m] != "\n":
                    m -= 1
                await ctx.send(wrap.format(code[n:m]))
                n += 1990
                m += 1990
        else:
            await ctx.send(wrap.format(code))

    @commands.command(hidden=True)
    @checks.is_owner()
    async def addowner(self, ctx, *, user: discord.Member):
        owner_data = data.read_json("data/owner.json")
        if user.id in owner_data["owners"]:
            return await ctx.send("That user is already an owner :no_entry:")
        owner_data["owners"].append(user.id)
        data.write_json("data/owner.json", owner_data)
        await ctx.send("Added {} as an owner".format(user))

    @commands.command(hidden=True)
    @checks.is_owner()
    async def removeowner(self, ctx, *, user: discord.Member):
        owner_data = data.read_json("data/owner.json")
        if user.id not in owner_data["owners"]:
            return await ctx.send("That user is not an owner :no_entry:")
        owner_data["owners"].remove(user.id)
        data.write_json("data/owner.json", owner_data)
        await ctx.send("Removed {} as an owner".format(user))

    @commands.command(hidden=True)
    @checks.is_owner()
    async def ownerlist(self, ctx):
        owner_data = data.read_json("data/owner.json")["owners"]
        s=discord.Embed(title="Owner List", description="\n".join([str(self.bot.get_user(x)) for x in owner_data]))
        await ctx.send(embed=s)

    @commands.command(hidden=True, name="as")
    @checks.is_owner()
    async def _as(self, ctx, user: str, command_name: str, *, args: str=""):
        user = await arg.get_member(ctx, user)
        if not user:
            return await ctx.send("You're retarded that's not a user :no_entry:")
        else:
            ctx.author = user
            ctx.message.author = user
        if " " in command_name:
            command = command_name.split(" ", 1)
            try:
                command = self.bot.all_commands[command[0]].all_commands[command[1]]
            except KeyError:
                return await ctx.send("Invalid command :no_entry:")
        else:
            try:
                command = self.bot.all_commands[command_name]
            except KeyError:
                return await ctx.send("Invalid command :no_entry:")
        ctx.message.content = ctx.prefix + command_name + " " + args
        ctx.view = StringView(args)
        await command.invoke(ctx)

    @commands.command(hidden=True)
    @checks.is_owner()
    async def disable(self, ctx, *, command: str):
        command = self.bot.get_command(command)
        if not command:
            return await ctx.send("Invalid command :no_entry:")
        command.enabled = not command.enabled
        if command.enabled == False:
            await ctx.send("`{}` has been disabled.".format(command))
        else:
            await ctx.send("`{}` has been enabled.".format(command))

    @commands.command(hidden=True)
    @checks.is_owner()
    async def blacklistuser(self, ctx, user: str):
        user = await arg.get_member(ctx, user)
        r.table("blacklist").insert({"id": "owner", "users": []}).run(durability="soft")
        data = r.table("blacklist").get("owner")
        if str(user.id) not in data["users"].run(durability="soft"):
            data.update({"users": r.row["users"].append(str(user.id))}).run(durability="soft")
            await ctx.send("{} has been blacklisted.".format(user))
        elif str(user.id) in data["users"].run(durability="soft"):
            data.update({"users": r.row["users"].difference([str(user.id)])}).run(durability="soft")
            await ctx.send("{} is no longer blacklisted".format(user))
		
    @commands.command(hidden=True)
    async def modules(self, ctx):
        unloaded, loaded = [], []
        list = [x.replace(".py", "") for x in os.listdir("cogs") if ".py" in x]
        for x in list:
            if not self.bot.get_cog(x):
                unloaded.append(x)
            else:
                loaded.append(x)
        s=discord.Embed(title="Modules ({})".format(len(list)))
        s.add_field(name="Loaded ({})".format(len(loaded)), value=", ".join(loaded) if loaded != [] else "None", inline=False)
        s.add_field(name="Unloaded ({})".format(len(unloaded)), value=", ".join(unloaded) if unloaded != [] else "None", inline=False)
        await ctx.send(embed=s)

    @commands.command(hidden=True)
    @checks.is_owner()
    async def updateavatar(self, ctx, *, url=None):
        if not url:
            if ctx.message.attachments:
                url = ctx.message.attachments[0].url
            else:
                await ctx.send("Provide a valid image :no_entry:")
                return
        avatar = requests.get(url).content
        try:
            await self.bot.user.edit(password=None, avatar=avatar)
        except:
            return await ctx.send("You've changed my profile picture too many times")
        await ctx.send("I have changed my profile picture")
		
    @commands.command(hidden=True)
    @checks.is_owner()
    async def shutdown(self, ctx):
        await ctx.send("Shutting down...")
        await self.bot.logout()
		
def setup(bot):
    bot.add_cog(owner(bot))