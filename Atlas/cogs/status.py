import os
import discord
from discord.ext import commands
from discord.utils import find
from random import randint
import asyncio
from utils import checks, arghelp
import rethinkdb as r
import uuid

class status:
    """Bot Status"""

    def __init__(self, bot):
        self.bot = bot
        self._task = bot.loop.create_task(self.display_status())

    def __unload(self):
        self._task.cancel()

    @commands.group(hidden=True)
    async def status(self, ctx):
        if ctx.invoked_subcommand is None:
            await arghelp.send(self.bot, ctx)
        else:
            data = r.table("botstats").get("stats")
            if "statuses" not in data.run():
                data.update({"statuses": []}).run(durability="soft")

    @status.command(hidden=True)
    @checks.is_owner()
    async def add(self, ctx, type: str, *, status: str):
        data = r.table("botstats").get("stats")
        if type.lower() in ["watching", "playing", "listening"]:
            type = type.lower()
            name = status
        else:
            name = type + status
            type = "playing"
        await ctx.send("Added `{}` as a status with `{}` as the status type".format(name, type))
        data.update({"statuses": r.row["statuses"].append({"id": str(uuid.uuid4()),"name": name, "type": type})}).run(durability="soft")

    @status.command(hidden=True)
    @checks.is_owner()
    async def remove(self, ctx, id: str):
        data = r.table("botstats").get("stats")
        if id not in data["statuses"].map(lambda x: x["id"]).run():
            return await ctx.send("That is not a valid status ID :no_entry:")
        await ctx.send("Removed that status.")
        data.update({"statuses": r.row["statuses"].filter(lambda x: x["id"] != id)}).run(durability="soft")

    @status.command(hidden=True)
    async def list(self, ctx):
        data = r.table("botstats").get("stats")["statuses"].run()
        if not data:
            return await ctx.send("No statuses have been set :no_entry:")
        s=discord.Embed()
        for x in data:
            s.add_field(name=x["id"], value="Type: {}\nStatus: {}".format(x["type"], x["name"]))
        await ctx.send(embed=s)

    async def display_status(self):
        i = 0
        while not self.bot.is_closed():
            try:
                data = r.table("botstats").get("stats").run()
                if not data["statuses"]:
                    statuses = ["{:,} users".format(len(self.bot.users))]
                else:
                    statuses = data["statuses"]
                new_status = statuses[i]
                await self.bot.change_presence(activity=discord.Activity(name=new_status["name"], type=getattr(discord.ActivityType, new_status["type"])))
            except Exception as e:
                await self.bot.get_channel(529763860870201365).send("```py\n" + traceback.format_exc() + "```")
            if i == len(statuses) - 1:
                i = 0
            else:
                i += 1
            await asyncio.sleep(120)

def setup(bot):
    n = status(bot)
    bot.add_cog(n)